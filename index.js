// console.log('js app is running');

let studentNameA = '2020-1923';
let studentNameB = '2020-2924';
let studentNameC = '2020-1925';
let studentNameD = '2020-1926';


// Declaration of an Array
//  Syntax --> let/cons arrayName = [elementA, elementB, elementC...];

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926'];


//Common example of arrays
let grades = [98.5, 94.3, 89.2, 90.1];



let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

let mixedArr = [12, "Asus", null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

let myTaks = [
    "drink html",
    'eat javascript',
    'inhale css',
    'bake sass'
];

let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1, city2, city3];

console.log(myTaks);
console.log(cities);

// Section Length Property
// The .length  property allows us to get and set the total number of items in an array.

let fullName = "Jamie Noble";
console.log(fullName.length);
console.log(cities.length);

// We use  .length-1 to delete 
myTaks.length = myTaks.length - 1;

console.log(myTaks.length);
console.log(myTaks);

//Another way to delete an item on an array
//Another sample using decrementation (--)
cities.length--;
console.log(cities);

fullName.length = fullName.length - 1;
console.log(fullName.length);
fullName.length--;
console.log(fullName.length); 


//Adding an item in an aray uysing incrementation(++)
let theBeatles = [ 
                    'John', 
                    'Paul', 
                    'Ringo', 
                    'George'
                 ];
theBeatles.length++;
theBeatles.length++;
theBeatles.length++;
console.log(theBeatles);


// Section Reading Arrays

console.log(grades[0]);
console.log(computerBrands[3]);

console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[4]);
console.log(lakersLegends[0])

lakersLegends
console.log("Array after re-assignment");
console.log(lakersLegends);





// Accessing the last element of an array
let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc']


let lastElementIndex = bullsLegends.length-1;

console.log(bullsLegends[lastElementIndex]);

//You can also add it directly
console.log(bullsLegends[bullsLegends.length-1]);


//Adding items to an array

let newArr = [];
console.log(newArr[0]);


newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);



newArr[newArr.length] = "Barett Wallace";
console.log(newArr);

//Looping over an array

for(let index = 0; index < newArr.length; index++){
  //You can use the loop counter as index to be able to show each array Items in the console
  console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40, 85,92,100]

for(let index = 0; index < numArr.length; index++){
  //You can use the loop counter as index to be able to show each array Items in the console
   if(numArr[index] % 5 === 0){
      console.log(numArr[index] + " is divided by 5");
   }else {
      console.log(numArr[index] + " is not divided by 5");
   }
}

// SECTION Multi demensional Array

let chessBoard = [
  ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
  ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
  ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
  ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
  ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
  ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
  ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
  ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

//[1] --> column
//[4] --> row
console.log([1][4])

console.log(chessBoard);
